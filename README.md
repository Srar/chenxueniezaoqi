# Installation for linux

```
yum groupinstall "Chinese Support"
yum groupinstall Fonts
```

Create a directory for font file.
```
mkdir -p /usr/share/fonts/ping-fang
```

Copy font file to directory.

```
cp ping-fang.ttf /usr/share/fonts/ping-fang
```

Update system fonts.

```
mkfontscale
mkfontdir
fc-cache -f -v
```