const fs = require("fs");
const path = require("path");

module.exports = class FileCache {

    constructor(directory) {
        this.directory = directory;
        if (!fs.existsSync(this.directory)) {
            fs.mkdirSync(this.directory);
        }
    }

    get(key, callback) {
        const filePath = path.join(this.directory, key);
        fs.exists(filePath, (exists) => {
            if (exists) {
                fs.readFile(filePath, callback);
            } else {
                callback(null, null);
            }
        });
    }

    set(key, data, callback) {
        const filePath = path.join(this.directory, key);
        fs.writeFile(filePath, data, callback);
    }

    delete(callback) {
        const filePath = path.join(this.directory, key);
        fs.unlink(filePath, callback);
    }
    
}

