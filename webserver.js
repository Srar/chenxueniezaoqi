
const argv = require("optimist")
    .usage("Usage: $0 --port [port]")
    .demand(["port"])
    .argv;

const fs = require("fs");
const crypto = require("crypto");
const morgan = require("morgan");
const express = require("express");
const bodyParser = require("body-parser");

const FileCache = require("./fileCache");
const imageCreater = require("./imageCreater");

const app = express();
app.enable("trust proxy");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan("short"));

const cache = new FileCache("cache");

app.get("/", (req, res) => {
    fs.readFile("tpl.html", function (err, buffer) {
        res.type("text/html;charset=utf-8");
        res.send(buffer);
    })
});

app.get("/create/:text", (req, res) => {
    const fullText = req.params["text"].toString();
    const extensionIndex = fullText.lastIndexOf(".png");

    if(extensionIndex === 0 || extensionIndex === -1) {
        res.status(503).send("boom!");
        return;
    }

    const text = fullText.substring(0, fullText.lastIndexOf(".png"));
    const cacheKey = crypto.createHash("md5").update(text).digest("hex");

    res.type("image/png");

    cache.get(cacheKey, (error, cachedData) => {
        if (cachedData) {
            res.header({ "Cache": "HIT" });
            res.send(cachedData);
            return;
        }
        const data = imageCreater(text);
        cache.set(cacheKey, data, function () {
            res.header({ "Cache": "MISS" });
            res.send(data);
        })
    })
});

app.listen(argv.port, (err) => {
    if (err) {
        console.error(err);
        process.exit(-1);
    }
    console.log("Fake image service listening on port " + argv.port)
});