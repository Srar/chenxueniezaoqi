const fs = require("fs");
const canvas = require("canvas");

const bgImage = new canvas.Image();
bgImage.src = fs.readFileSync("images/bg.png");

const leftImage = new canvas.Image();
leftImage.src = fs.readFileSync("images/left.png");

const topImage = new canvas.Image();
topImage.src = fs.readFileSync("images/top.png");

const textImage = new canvas.Image();
textImage.src = fs.readFileSync("images/text.png");

const textLeftImage = new canvas.Image();
textLeftImage.src = fs.readFileSync("images/textLeft.png");

const textCenterImage = new canvas.Image();
textCenterImage.src = fs.readFileSync("images/textCenter.png");

const textRightImage = new canvas.Image();
textRightImage.src = fs.readFileSync("images/textRight.png");

const textStyle = {
    fillStyle: "rgb(205, 174, 136)",
    font: '31px "PingFang SC"'
}

function getTextSize(text, textStyle) {
    const ctx = new canvas(1000, 70).getContext("2d");
    Object.assign(ctx, textStyle);
    const measuredData = ctx.measureText(text);
    return measuredData;
}

module.exports = function (text) {

    const measuredData = getTextSize(text, textStyle);
    let canvasWidth = 111 + 55 + measuredData.width + 40;
    if (canvasWidth < 450) {
        canvasWidth = 450;
    }

    const ctx = new canvas(canvasWidth, 170).getContext("2d");
    ctx.drawImage(bgImage, 0, 0, bgImage.width, bgImage.height);
    ctx.drawImage(leftImage, 0, 0, leftImage.width, leftImage.height);
    ctx.drawImage(topImage, 111, 0, topImage.width, topImage.height);
    ctx.drawImage(textLeftImage, 111, 62, textLeftImage.width, textLeftImage.height);

    Object.assign(ctx, textStyle);

    const textWidth = measuredData.width - 30;
    const textEmHeightDescent = measuredData.emHeightDescent;

    ctx.drawImage(textCenterImage, 111 + 63, 62, textCenterImage.width, textCenterImage.height);
    ctx.drawImage(textCenterImage, 111 + 63, 62, textCenterImage.width + textWidth, textCenterImage.height);
    ctx.drawImage(textRightImage, 111 + 55 + textWidth, 62, textRightImage.width, textRightImage.height);

    /* when chinese is contained */
    if (textEmHeightDescent > 0) {
        ctx.fillText(text, 152, 113);
    } else {
        ctx.fillText(text, 152, 122);
    }

    return ctx.canvas.toBuffer();
}